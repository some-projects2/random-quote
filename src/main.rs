use structopt::StructOpt;
use hyper::Client;
use hyper_tls::HttpsConnector;
use serde::{Serialize, Deserialize};

#[derive(Debug, StructOpt)]
#[structopt(name = "random_quote", about = "A random quote generator")]
struct Options{
}

#[derive(Deserialize, Serialize, Debug)]
struct Quote {
    q: String,
    a: String,
    h: String
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    let _options = Options::from_args();
    let https = HttpsConnector::new();
    let client = Client::builder().build::<_, hyper::Body>(https);

    let uri = "https://zenquotes.io/api/random".parse().unwrap();

    let res = client.get(uri).await.unwrap();

    println!("Response: {}", res.status());

    //while let Some(chunk) = res.body_mut().data().await {
    //    stdout().write_all(&chunk?).await?;
    //}

    let body_bytes = hyper::body::to_bytes(res.into_body()).await?;
    let quote: Vec<Quote> = serde_json::from_slice(&body_bytes).unwrap();
    println!("\n{}", quote[0].q);
    println!("- {}\n", quote[0].a);

    Ok(())
}


